(import test)
(import (rename sum-nums (sum_v1 sum)))

(test-group
 "when input is an empty list"
 (test
  "should return 0"
  0
  (sum '())))

(test-group
 "when input is a single-element list"
 (test-group
  "should return that element"
  (test 0 (sum '(0)))
  (test 1 (sum '(1)))
  (test -42 (sum '(-42)))))

(test-group
 "when input contains two or more elements"
 (test-group
  "should return the correct sum"
  (test (+ -1 -2) (sum '(-1 -2)))
  (test (+ 100 1 2 3 4) (sum '(100 1 2 3 4)))
  (test (+ 42.3 -42.3) (sum '(42.3 -42.3)))))
