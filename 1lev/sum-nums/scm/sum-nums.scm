(module sum-nums (sum_v1)
  (import scheme)

  (define (sum_v1 lon)
    (let go ((lst lon))
      (if (null? lst)
          0
          (+ (car lst)
             (go (cdr lst)))))))
