(module palindrome (palind-v1? palind-v2?)
  (import scheme)
  (import (chicken base))
  (import (only utf8 string-length string-substitute))
  (import (only srfi-13 string-downcase))

  ;;
  ;; String -> Bool
  ;; Checks wheter string ‘s’ is a palindrome.
  ;;
  ;; ASSUME: input contains no spaces or punctuation characters.
  ;;
  (define (palind-v1? s)
    (cond
     ((<= (string-length s) 1) #t)
     ((not (char=?
            (string-ref s 0)
            (string-ref s (sub1 (string-length s)))))
      #f)
     (else
      (palind-v1? (substring s 1 (sub1 (string-length s)))))))

  ;;
  ;; String -> Bool
  ;; Checks wheter string ‘s’ is a palindrome.
  ;;
  ;; This function removes non-alphabetic, ascii chars.
  ;;
  (define (palind-v2? str)
    (let ((cleaned-str
           (string-downcase
            (string-substitute "( |\\?)" "" str 'g))))

      (let loop ((s cleaned-str))
        (cond
         ((<= (string-length s) 1) #t)
         ((not (char=?
                (string-ref s 0)
                (string-ref s (sub1 (string-length s))))) #f)
         (else
          (loop (substring s 1 (sub1 (string-length s)))))))))
)


