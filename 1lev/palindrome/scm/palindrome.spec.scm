(import test)
(import
  (rename palindrome
          ;(palind-v1? palind?)
          (palind-v2? palind?)))

(test #t (palind? ""))
(test #t (palind? "z"))
(test #t (palind? "λ"))

(test #f (palind? "yz"))
(test #t (palind? "zz"))

(test #f (palind? "car"))
(test #t (palind? "ana"))

(test #f (palind? "n00b"))
(test #t (palind? "b00b"))

(test #t (palind? "racecar"))

(test #t (palind? "Was it a car or a cat I saw?"))

