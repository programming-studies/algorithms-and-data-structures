(module fact (fact_v1)
  (import scheme)

  ;;;;
  ;; Int -> Int
  ;; Computes the factorial of ‘n’.
  ;;
  (define (fact_v1 n)
    (if (= n 1) 1
        (* n (fact_v1 (- n 1)))))
  )

