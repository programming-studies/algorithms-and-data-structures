(import test)
(import (rename fact (fact_v1 fact)))

(load "fact.scm")

(test "fact test" 1 (fact 1))

(test "fact 3" (* 3 2 1) (fact 3)) ; 6

(test "fact 4" (* 4 3 2 1) (fact 4)) ; 24

(test "fact 5" (* 5 4 3 2 1) (fact 5)) ; 120
