# Algorithms and Data Structures

- [Algorithms and Data Structures](#algorithms-and-data-structures)
  - [Algorithms Difficulty](#algorithms-difficulty)
  - [Languages](#languages)
    - [C](#c)
    - [Scheme](#scheme)
    - [JavaScript](#javascript)
    - [TypeScript](#typescript)
    - [Haskell](#haskell)
    - [Ruby](#ruby)
  - [Directories](#directories)

My solutions, notes and ideas on solving algorithms and data structures, either from online challenge sites or from stuff I came across while working or playing with something.

## Algorithms Difficulty

Directories are named from `1lev` (the easiest challenges) to `5lev` (the hardest challenges). We may have higher levels in the future. Let's start with easier stuff :)

## Languages

It is my goal to keep learning and practicing on several different languages and programming paradigms, therefore, I try to solve exercises in at least the languages I shortly describe bellow.

### C

Procedural, low level (if compared with others), with manual memory management, pointers, and that kind of stuff.


### Scheme

Multi-paradigm language of the Lisp family. I use [Chicken Scheme](http://wiki.call-cc.org) in this project.

### JavaScript

Multi-paradigm language which allows for a heavy FP style.

I use JavaScript (ECMAScript) almost everyday since I started web development, and even if I use other languages that compile to it, it is a must not to lose touch with it anyway.

### TypeScript

Multi-paradigm language which compiles to JavaScript. Enforces consistency other stuff which vanilla JS does not.

I use TypeScript through [Deno](https://deno.land/) in this project.

### Haskell

A statically typed, purely functional programming language.

### Ruby

A delightful dynamically typed object oriented programming language.

## Directories

I use the file extension for each language as the directories, therefore:

- `c` for the C programming language;
- `scm` for Chicken Scheme;
- `js` for JavaScript (ECMAScript);
- `ts` for TypeScript;
- `hs` for Haskell;
- `rb` for Ruby;
